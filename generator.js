const faker = require('faker')
const lodash = require('lodash')


const testWords = [
    'Динозавры',
    'Протоирей Смиронов',
    'Мой писюн',
]

function generateTestData() {
    return lodash.times(
        testWords.length,
        (index) => ({
            id: index,
            title: testWords[index],
        }),
    )
}


module.exports = {
    testData: generateTestData(),
}
