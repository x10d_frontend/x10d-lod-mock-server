const express = require('express')
const bodyParser = require('body-parser')

const generatedData = require('./generator')

const PORT = 8888
const app = express()

app.use(bodyParser.json())


app.get(`/test`, (req, res) => {
    return res.json(generatedData.testData)
})


app.listen(PORT, () => {
    console.log(`> Ready on http://localhost:${PORT}`)
})
